import express from 'express'

require('@google-cloud/debug-agent').start({ serviceContext: { enableCanary: false } })

const app = express()

app.get('/gitlab-ci.yml', (req, res) => {
  console.log(req.headers)
  res.json({
    image: 'alpine:latest',
    stages: ['test', 'build'],
    test: {
      stage: 'test',
      script: 'echo $CI_MERGE_REQUEST_ID',
      artifacts: { reports: { junit: ['junit.xml'] } },
    },
    build: { stage: 'build', script: 'exit 1' },
  })
})
app.get('/json_schema.json', (req, res) => {
  res.json(require('../schemas/configuration-schema.json'))
})

const port = process.env.SERVER_PORT || 8080
app.listen(port, () => console.log(`Server started on port ${port}`))
