const glob = require('glob')
const { matchers } = require('jest-json-schema')
expect.extend(matchers)

const schema = require('../../../schemas/configuration-schema.json')
const root_folder = 'src/tests/json_schema'

describe('Tests valides', () => {
  const fichiersValides = glob.sync(root_folder + '/valides/**/*.json')
  fichiersValides.forEach(fichier => {
    it(fichier, () => {
      expect(require(fichier.replace(root_folder, '.'))).toMatchSchema(schema)
    })
  })
})

describe('Tests invalides', () => {
  const fichiersInvalides = glob.sync(root_folder + '/invalides/**/*.json')
  fichiersInvalides.forEach(fichier => {
    it(fichier, () => {
      expect(require(fichier.replace(root_folder, '.'))).not.toMatchSchema(schema)
    })
  })
})
